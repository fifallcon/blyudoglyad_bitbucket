import psycopg2
import cv2
import numpy as np



class ImageHandler:
   def __init__(self, image, size):
      self.image = image
      self.image_size = size


   def resizeImg(self, img, w, h):
      height, width = img.shape[0], img.shape[1]
      new_height = int(w * height / width)
      if new_height > h:
         new_width = int(h * width / height)
         return cv2.resize(img, (new_width, h))
      else:
         return cv2.resize(img, (w, new_height))


   def save_many_photo(self, img, angle_rotation = 360):
      height, width = img.shape[0], img.shape[1]
      circle_img = np.zeros((height, width), np.uint8)
      cv2.circle(circle_img, (int(width / 2), int(height / 2)), int(height / 2), 1, thickness = -1)
      img = cv2.bitwise_and(img, img, mask = circle_img)

      for j in range(angle_rotation):
         rows, cols = img.shape[0], img.shape[1]
         M = cv2.getRotationMatrix2D((cols / 2, rows / 2), j, 1)
         dst = cv2.warpAffine(img, M, (cols, rows))

         dst = self.resizeImg(dst, self.image_size, self.image_size)
         arr = []
         for row in dst:
            for pix in row:
               arr.append(int(pix[0]))
         print(len(arr))



         # cv2.imwrite("miniDB\\im_{0}_{1}.jpg".format(str(i).zfill(3), str(j).zfill(3)), dst)

   def image_upload(self):
      conn = psycopg2.connect(
         "dbname='sbiskassir' user='postgres' host='10.76.165.81' password='postgre'")
      cursor = conn.cursor()

      cursor.execute("""SELECT image FROM images WHERE label_id=14""")

      k = 0
      for row in cursor.fetchall():
         array = row[0]
         print(type(array))
         print(len(array))

         height = self.image_size
         width = self.image_size

         blak_image = np.zeros((height, width, 3), np.uint8)
         i = 0
         for row in blak_image:
            j = 0
            for col in row:
               pix = array[i * self.image_size + j]
               rgbpix = [pix, pix, pix]
               blak_image[i, j] = rgbpix
               j += 1
            i += 1

         k += 1
         save_many_photo(blak_image, k)

      cv2.waitKey(0)
      cv2.destroyAllWindows()
