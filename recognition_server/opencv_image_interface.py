import cv2
import numpy as np
import psycopg2
import math
import utils


class bg_image():
    @staticmethod
    def getTypeList():
        query = """SELECT "@Dish" FROM "Dish" """

        query_result = utils.db.sql_query_result(query)
        result = []
        for row in query_result:
            result.append(row[0])
        return result

    @staticmethod
    def getImageList(id):
        query = "SELECT id FROM images WHERE label_id = {0}".format(id)

        query_result = utils.db.sql_query_result(query)
        result = []
        for row in query_result:
            result.append(row[0])
        return result

    def __init__(self, id, size):
        query = "SELECT image FROM images WHERE id = {0}".format(id)
        query_result = utils.db.sql_query_result(query)
        # ourArra
        array = query_result[0][0]

        height = int(math.sqrt(len(array)))
        width = height

        self.image = np.zeros((height, width, 3), np.uint8)
        i = 0
        k = 0
        for row in self.image:
            j = 0
            for col in row:
                pix = array[i * width + j]
                rgbpix = [pix, pix, pix]
                self.image[i, j] = rgbpix
                j += 1
            i += 1

        k += 1
        self.image = utils.resizeImg(self.image, size, size)

    def getCvImage(self, angle=0):
        rows, cols = self.image.shape[0], self.image.shape[1]
        M = cv2.getRotationMatrix2D((cols / 2, rows / 2), angle, 1)
        dst = cv2.warpAffine(self.image, M, (cols, rows))
        return dst

    def getImage(self, angle=0):
        rows, cols = self.image.shape[0], self.image.shape[1]
        M = cv2.getRotationMatrix2D((cols / 2, rows / 2), angle, 1)
        dst = cv2.warpAffine(self.image, M, (cols, rows))

        arr = []
        for row in dst:
            for pix in row:
                arr.append(int(pix[0]))
        return arr

    @staticmethod
    def based_to_db(label_id=None, type_id=None):
        cap = cv2.VideoCapture(0)
        ret, frame = cap.read()
        cv2.waitKey(0)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        gray_blur = cv2.GaussianBlur(gray, (9, 9), 20)

        circles = cv2.HoughCircles(gray_blur, cv2.HOUGH_GRADIENT, 1.2, 30)
        # ensure at least some circles were found
        if circles is not None:
            # circles = np.round(circles[0, :]).astype("int")
            circles = utils.getCircles(circles)
            # loop over the (x, y) coordinates and radius of the circles
            for (x, y, r) in circles:
                crop_img = gray[y - r: y + r, x - r: x + r]
                print(str(crop_img))
                crop_img = utils.removeBackground(crop_img)
                try:
                    utils.save_to_db(crop_img, label_id, type_id)
                except Exception as e:
                    raise e
        else:
            print("circle 404")


    @staticmethod
    def parse(base64):
        result = []
        img = utils.b64ToImg(base64)
        if img is None:
            return ''
        # cv2.imshow('a', img)
        # cv2.waitKey(0)
        
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        gray_blur = cv2.GaussianBlur(gray, (9, 9), 20)

        circles = cv2.HoughCircles(gray_blur, cv2.HOUGH_GRADIENT, 1.2, 30)
        # ensure at least some circles were found
        if circles is not None:
            # circles = np.round(circles[0, :]).astype("int")
            circles = utils.getCircles(circles)
            # loop over the (x, y) coordinates and radius of the circles
            i = 0
            for (x, y, r) in circles:
                print("x:=" + str(x))
                crop_img = img[y - r: y + r, x - r: x + r]
                crop_img_gs = gray[y - r: y + r, x - r: x + r]
                crop_img_gs = utils.removeBackground(crop_img_gs)
                try:
                    id = utils.save_to_db(crop_img_gs)
                    result.append(
                        {
                            "ID":   i,
                            "@Dish":   id, 
                            "x" :   x-r, 
                            "y" :   y-r, 
                            "w" :   2*r, 
                            "h" :   2*r,
                            "b64" : utils.imgToB64(crop_img)
                        }
                    ) 
                except Exception as e:
                    raise e
                i+=1
        # print(result)
        return result