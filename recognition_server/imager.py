from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from database import db as database

db = database('85.21.233.63', 7777, 'sbiskassir', 'postgres', 'kassir123')



class DataSet():
   def __init__(self, type_name):
      if db:
         type_list = {'100':1, '200':3}

         self.list_image = []
         self.list_label = []

         self.test_image = []
         self.test_label = []

         self.get_images(type_list[type_name])
         print(str(len(self.list_label)))
         print(str(len(self.list_image)))
         print(str(len(self.test_image)))
         print(str(len(self.test_label)))

      else:
         raise Exception('[data_set] connect to db fail!!')


   def add_label(self, name):
      query = '''
      INSERT INTO label(label) VALUES ('{label}')
      ON CONFLICT (label)
         DO UPDATE SET label = '{label}'
       RETURNING id
      '''.format(label = name)
      lid = db.sql_query_result(query)
      if lid:
         return lid[0][0]


   def get_label_name(self, id):
      query = '''
      SELECT label FROM labels WHERE id = {id}
      '''.format(id = id)
      return db.sql_query_result(query)[0][0]


   def add_image(self, pixel_list, label):
      label_id = self.add_label(label)
      query = '''
      INSERT INTO images(image, label_id)
      VALUES (ARRAY {plist}, {label_id})
      '''.format(plist = pixel_list,
                 label_id = label_id)
      db.sql_query_result(query)

      self.list_image.append(pixel_list)
      self.list_label.append(label_id)

   def morph_pixel(self, num):
      return 1 if num > 127 else 0

   def get_images(self, type_id):
      query = '''
      SELECT image, label_id, type_id
      FROM images
      WHERE type_id IN ({type}, {type} + 1)
      '''.format(type = type_id)

      dataset = db.sql_query_result(query)
      if dataset:
         for row in dataset:

            image = row[0]
            label = row[1]
            type_id = row[2]

            # image = [self.morph_pixel(i) for i in image]

            l = len(image)
            if l != 10000:
               image = image + [0 for _ in range(10000 - l)]

            llb = [0 for _ in range(13)]
            llb[label - 1] = 1
            if type_id % 2 == 0:
               self.test_image.append(image)
               self.test_label.append(llb)
            else:
               self.list_image.append(image)
               self.list_label.append(llb)


   def get_batch(self):
      return self.list_image, self.list_label
