from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler
from opencv_image_interface import bg_image
# Restrict to a particular path.
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)

# Create server
server = SimpleXMLRPCServer(("localhost", 8000),
                            requestHandler=RequestHandler)
server.register_introspection_functions()

# Register an instance; all the methods of the instance are
# published as XML-RPC methods (in this case, just 'mul').
def parse(b64):
	print('SDGDFGDFGDFGDFGD')
	print(len(b64))
	return bg_image.parse(b64)
server.register_function(parse)

# Run the server's main loop
server.serve_forever()