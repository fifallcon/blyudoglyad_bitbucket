from opencv_image_interface import bg_image
import tensorflow as tf
import numpy as np
import datetime


class ANN:
    def __init__(self, name):
        self.name = name

        self.image_size = int(name)
        self.blob_size = self.image_size * self.image_size
        self.train_angls = 100

        self.full_data_list = []
        self.full_label_list= []

        self.trane_data_list = []
        self.trane_lable_list = []

        self.test_data_list = []
        self.test_lable_list = []

        self.label_list = bg_image.getTypeList()
        self.label_list.sort()
        self.label_count = len(self.label_list)

        self.y_ = None

        self.loads()

        self.train_all()

        self.recognition()

    def loads(self):
        def handler(label):
            for image_id in bg_image.getImageList(label):
                image = bg_image(image_id, self.image_size)
                for angle in range(self.train_angls):
                    trane_data = image.getImage(angle)
                    self.full_data_list.append(trane_data)
                    if len(trane_data) != 10000:
                        print('Fail')
                    self.full_label_list.append(self.get_label_list(label))

        for label in self.label_list:
            handler(label)
        print(str(len(self.trane_data_list)))
        print(str(len(self.trane_lable_list)))

        self.trane_data_list = self.full_data_list[4:]
        self.trane_lable_list = self.full_label_list[4:]

        self.test_data_list = self.full_data_list[:4]
        self.test_lable_list = self.full_label_list[:4]


    def train_all(self):

        self.train_network()

    def train_extend(self):
        pass

    def get_id_by_label(self, label):
        return self.label_list.index(label)

    def get_label_by_id(self, id):
        return self.label_list[id]

    def get_label_list(self, label):
        llb = [0 for _ in range(self.label_count)]
        pose = self.get_id_by_label(label)
        llb[pose] = 1
        return llb

    def train_network(self):
        self.x = tf.placeholder(tf.float32, [None, self.blob_size])
        self.W = tf.Variable(tf.zeros([self.blob_size, self.label_count]))
        self.b = tf.Variable(tf.zeros([self.label_count]))
        self.y = tf.matmul(self.x, self.W) + self.b

        # Define loss and optimizer
        self.y_ = tf.placeholder(tf.float32, [None, self.label_count])
        cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(self.y, self.y_))
        train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)
        self.sess = tf.InteractiveSession()

        # Train

        dt = datetime.datetime.now()

        tf.initialize_all_variables().run()
        for i in range(100):
           print(str(i))
           # batch_xs, batch_ys = data_set.get_batch()
           self.sess.run(train_step, feed_dict = {self.x: self.trane_data_list, self.y_: self.trane_lable_list})

        print(str(datetime.datetime.now() - dt))



    def recognition(self):
        keep_prob = tf.placeholder(tf.float32)
        a = self.sess.run(tf.argmax(self.y, 1), feed_dict={self.x: self.test_data_list, keep_prob: 1.0})
        for i in a:
            print(self.get_label_by_id(i))
