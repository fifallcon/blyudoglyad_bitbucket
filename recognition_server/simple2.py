import xmlrpc.client
import utils
import cv2
import base64

with utils.Profiler() as p:
    s = xmlrpc.client.ServerProxy('http://10.76.174.1:1111')
    # img = cv2.imread('1.jpg')
    # b64 = utils.imgToB64(img)
    with open('3.png', "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    some = s.ParseImg(encoded_string.decode('utf-8'))
    print(some)