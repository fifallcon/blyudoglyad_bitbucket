# -*- coding: utf-8 -*-
import psycopg2



class db:
   def __init__(self, host, port, dbname, user, password):
      self.host = host
      self.port = int(port)
      self.dbname = dbname
      self.user = user
      self.password = password


   def connect_to_postgres(self):
      try:
         connection = psycopg2.connect(
            host = self.host,
            port = self.port,
            database = self.dbname,
            user = self.user,
            password = self.password)
         print ('connect to database [{}] is success'.format(self.dbname))
         connection.autocommit = True
         return connection
      except Exception as e:
         print ("fail connect to database [{}]. {}""".format(self.dbname, str(e)))


   def sql_query_result(self, query):
      conn = self.connect_to_postgres()
      cursor = conn.cursor()
      cursor.execute(query)
      if cursor.rowcount > 0:
         res = cursor.fetchall()
         cursor.close()
         conn.close()
         return res
      else:
         cursor.close()
         conn.close()
         return []
