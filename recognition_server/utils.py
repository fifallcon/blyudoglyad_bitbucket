import cv2
import base64
import sys
import database
import time
import numpy as np

db = database.db('91.232.92.53', '5432', 'sbiskassir', 'postgres', 'postgre')


class Profiler(object):
    def __enter__(self):
        self._startTime = time.time()
    def __exit__(self, type, value, traceback):
        print("Elapsed time: {:.3f} sec".format(time.time() - self._startTime))

def imgToB64(img):
    cnt = cv2.imencode('.png',img)[1]
    b64 = base64.encodestring(cnt.tobytes())
    return b64.decode('utf-8')

def b64ToImg(b64):
    """ Convert base64 string to image """
    img = base64.b64decode(b64)
    npimg = np.fromstring(img, dtype=np.uint8)
    img_np = cv2.imdecode(npimg, 1)
    return img_np
#
def getCircles(circles):
    if circles is not None:
        circles = np.round(circles[0, :]).astype("int")
        lc = circles.shape[0]
        r = np.floor(lc / 2) - 1
        r = np.round(r).astype("int")
        for k in range(0, lc - 1):
            for y in range(0, ):
                if (circles[k, 0] - circles[k + y], 0) ** 2 + (circles[k, 1] - circles[k + y], 1) ** 2 < circles(k,
                                                                                                                 2) ** 2:
                    np.delete(circles, k, 0)
    return circles


def removeBackground(crop_img):
    h = crop_img.shape[0]
    mask = np.ones((h, h))
    mask = np.round(mask).astype("uint8")
    for i in range(0, h):
        for j in range(0, h):
            if (i - h / 2) ** 2 + (j - h / 2) ** 2 > (h / 2) ** 2:
                mask[i, j] = 0
    gray = crop_img * mask
    return gray


def save_to_db(crop_img, label_id=None, type_id=None):
    arr = []
    for row in crop_img:
        for pix in row:
            arr.append(int(pix))
    if label_id and type_id: 
      query = '''
                  INSERT INTO images(image, label_id, type)
                  VALUES (ARRAY {plist}, {label_id}, {type_id})
                  returning id
                  '''.format(plist=arr,
                             label_id=label_id, type_id=type_id)
    else:
      query = ''' 
         INSERT INTO images(image, label_id, type)
         VALUES (ARRAY {plist}, null, null)
         returning id
                  '''.format(plist=arr)
    return db.sql_query_result(query)[0][0]

def resizeImg(img, w, h):
        height, width = img.shape[0], img.shape[1]
        new_height = int(w * height / width)
        if new_height > h:
            new_width = int(h * width / height)
            return cv2.resize(img, (new_width, h))
        else:
            return cv2.resize(img, (w, new_height))
