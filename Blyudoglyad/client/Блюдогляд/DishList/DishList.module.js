/**
 * @author student
 */
define('js!SBIS3.BLYUDOGLYAD.DishList', 
['js!SBIS3.CORE.CompoundControl',
'html!SBIS3.BLYUDOGLYAD.DishList',
'js!WS.Data/Source/SbisService',
'css!SBIS3.BLYUDOGLYAD.DishList',
'js!SBIS3.CONTROLS.Button',
'js!SBIS3.CONTROLS.DataGridView'],
function(CompoundControl, dotTplFn, SbisService) {
   /**
    * SBIS3.BLYUDOGLYAD.DishList
    * @class SBIS3.BLYUDOGLYAD.DishList
    * @extends $ws.proto.CompoundControl
    */
   var moduleClass = CompoundControl.extend(/** @lends SBIS3.BLYUDOGLYAD.DishList.prototype */{
      _dotTplFn: dotTplFn,
      $protected: {
         _options: {
            rec_id: null,
            callback: null
         }
      },
      $constructor: function() {
      },

      init: function() {
         moduleClass.superclass.init.call(this);
         
         var self = this;
         var view = this.getChildControlByName('browserView');
         
         var dataSource = new SbisService({
            endpoint: 'Dish',
            idProperty: '@Dish',
            binding: {
               query: 'List'
            }
         });
         view.setDataSource(dataSource);
         
         var label = null;
         var name = null;
         var cost = null;
         var filter = new $ws.proto.Record();
         //filter.addColumn( "ИдО", $ws.proto.Record.FIELD_TYPE_STRING );
         //filter.set( "ИдО", 24592 );
         //new $ws.proto.BLObject( "Dish" ).call( "List", { "Название": Name, "Цена": Cost },
         //$ws.proto.BLObject.RETURN_TYPE_RECORDSET ).addBoth( function( pet_rec ){ res = pet_rec } );
      
         view.subscribe("onItemClick", function(event, selected){
            var ch_rec = self.getTopParent().getLinkedContext().getValue("rs").getRecordByPrimaryKey(self._options.rec_id);
            var sel_dish = parseInt(selected);
            ch_rec.set("@Dish", sel_dish);
            
            new $ws.proto.BLObject( "Dish" ).call( "List", { "Навигация": null, "Сортировка": null, "ДопПоля": null, "Фильтр": new $ws.proto.Record() }, $ws.proto.BLObject.RETURN_TYPE_RECORDSET ).addCallback(function(list_rs){
               var new_rec = list_rs.getRecordByPrimaryKey(sel_dish);
               ch_rec.set("Name", new_rec.get("Name"));
               ch_rec.set("Cost", new_rec.get("Cost"));
               self._options.callback(self._options.rec_id);
            });
            
            self.sendCommand('close', view.getSelectedItems()._$items);
         });
         self.getChildControlByName("browserView").subscribe('onDrawItems', function(eventObject) {
            document.getElementsByClassName("ws-absolute ws-window radius ws-modal ws-window-draggable")[0].className += ' modal_block';
            $('.ws-window-titlebar').css('background', '#8991a9');
            $('.controls-DataGridView__th').addClass('modal_table_titlebar');
            $('.controls-DataGridView__columnValue').addClass('modal_table_th');
            document.getElementsByClassName("controls-DataGridView__table")[0].className = 'modal_table';
            var length = $('div.controls-DataGridView__columnValue').length;
            var height = 45 * length / 2 + 40;
            $('.modal_table').css('height', height + 'px');
            for (var i = 0; i < length; i++){
               if (i % 2 != 0)
                  $('div.controls-DataGridView__columnValue')[i].innerText += ' руб';
            }
         });
      }
      
   });

   return moduleClass;
});
