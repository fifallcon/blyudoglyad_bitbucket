/**
 * @author Шергесов
 */
define('js!SBIS3.BLYUDOGLYAD.Theme', ['js!SBIS3.CORE.CompoundControl', 'html!SBIS3.BLYUDOGLYAD.Theme', 'css!SBIS3.BLYUDOGLYAD.Theme'], function(CompoundControl, dotTplFn) {
   var moduleClass = CompoundControl.extend({
      _dotTplFn: dotTplFn,
      init: function() {
         moduleClass.superclass.init.call(this);
      }
   });

   return moduleClass;
});