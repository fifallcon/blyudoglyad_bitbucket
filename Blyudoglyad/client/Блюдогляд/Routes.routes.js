module.exports = function(Component){
   var render = function render(req, res, innerControl){
      res.render('SBIS3.BLYUDOGLYAD.Theme', {
         'content': new Component(innerControl, {}),
         'title' : 'Кассир/СБИС'
      }); 
   };
   return {
      '/' : function(req, res){
         render(req, res, 'SBIS3.BLYUDOGLYAD.StartPage');
      },
      '/index.html' : function(req, res){
         render(req, res, 'SBIS3.BLYUDOGLYAD.StartPage');
      }
   }
};