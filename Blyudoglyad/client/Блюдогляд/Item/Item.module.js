/**
 * @author Шергесов Н.Н.
 */
define('js!SBIS3.BLYUDOGLYAD.Item', ['js!SBIS3.CORE.CompoundControl',
'html!SBIS3.BLYUDOGLYAD.Item',
'css!SBIS3.BLYUDOGLYAD.Item',
'js!SBIS3.BLYUDOGLYAD.DishList',
'js!SBIS3.CORE.TemplatedArea',
'js!SBIS3.CONTROLS.Button'
], function(CompoundControl, dotTplFn, css, DishList, TemplArea) {
   /**
    * SBIS3.BLYUDOGLYAD.Item
    * @class SBIS3.BLYUDOGLYAD.Item
    * @extends $ws.proto.CompoundControl
    * @control
    */
   var moduleClass = CompoundControl.extend(/** @lends SBIS3.BLYUDOGLYAD.Item.prototype */{
      _dotTplFn: dotTplFn,
      $protected: {
         _options: {
            id: null,
            list: null
         }
      },
      $constructor: function() {
      },
      
      update: function(self, rec){
         self.getContainer().find("#item_info").empty();
         self.getContainer().find("#item_info").append(rec.get("Name")); 
         self.getContainer().find("#item_priceName").empty();
         self.getContainer().find("#item_priceName").append(rec.get("Cost") + " р.");
         self.getContainer().find("#item_img").attr("src", "data:image/png;base64," + rec.get("b64"));
      },
      
      init: function() {
         moduleClass.superclass.init.call(this);
         var self = this;
         var rec = this._options.list.getRecords()[this._options.id];
         moduleClass.prototype.update(this, rec);
         
         this.getChildControlByName('ItemChange').subscribe('onActivated', function(eventObject) {
            $ws.core.attachInstance( 'Control/Area:DialogSelector', {
               template: "js!SBIS3.BLYUDOGLYAD.DishList",
               resizeable: false,
               multiSelect: false,
               context: self.getLinkedContext(),
               componentOptions: {
                  rec_id: self._options.id,
                  callback: function(new_id){
                     moduleClass.prototype.update(self, self.getLinkedContext().getValue("rs").getRecordByPrimaryKey(new_id));
                     $ws.single.ControlStorage.getByName("ResBill").reload();
                  }
               }
            });
         });
         
      
      }
   });

   return moduleClass;
});