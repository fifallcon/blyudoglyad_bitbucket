/**
 * @author Шергесов Н.Н.
 */
define('js!SBIS3.BLYUDOGLYAD.StartPage',
[
'js!SBIS3.CORE.CompoundControl',
'html!SBIS3.BLYUDOGLYAD.StartPage',
'css!SBIS3.BLYUDOGLYAD.StartPage',
'js!SBIS3.BLYUDOGLYAD.Item',
'js!SBIS3.BLYUDOGLYAD.Bill',
'js!SBIS3.CORE.TemplatedArea',
'js!SBIS3.CONTROLS.Button'
],
function(CompoundControl, dotTplFn, css, recognizedItem, Bill) {
   /**
    * SBIS3.BLYUDOGLYAD.StartPage
    * @class SBIS3.BLYUDOGLYAD.StartPage
    * @extends $ws.proto.CompoundControl
    * @control
    * @public
    */
   var moduleClass = CompoundControl.extend(/** @lends SBIS3.BLYUDOGLYAD.StartPage.prototype */{
      _dotTplFn: dotTplFn,
      $protected: {
         _options: {
            className: 'StartPage',
         },
         _video : null,
         _canvas : null,
      },
      $constructor: function() {
      },

      init: function() {
         moduleClass.superclass.init.call(this);
         
         var self = this;
         this._video = document.getElementById('video_tag');
         this._canvas = document.getElementById('canvas');
         this._canvas.hidden = true;
         document.getElementById('recognized').hidden = true;
         
         navigator.getUserMedia( {video: true}, function (stream) {
            self._video.src = URL.createObjectURL(stream);
            self.getChildControlByName('GetPhoto').show();
         }, function () {
            $ws.core.alert("Не удалось захватить камеру").addCallback(function(){
               location.reload();
            });
         });
         
         this.getChildControlByName('GetPhoto').subscribe('onActivated', function(eventObject) {
            this.hide();
            
            $(self._canvas).css("display", "block");
            self._canvas.hidden = false;
            self._video.hidden = true;
            
            var cont = self._canvas.getContext('2d');
            var height = self._video.videoHeight;
            var width = self._video.videoWidth;
            
            canvas.width = width;
            canvas.height = height;
            cont.fillRect(0, 0, width, height);
            cont.drawImage(self._video, 0, 0, width, height);
            
            self._canvas.toBlob(function(based64blob){
               var reader = new window.FileReader();
               reader.readAsDataURL(based64blob);
               reader.onloadend = function() {
                  var base64data = reader.result;
                  base64data = base64data.split(",")[1];
                  $ws.single.Indicator.setMessage("Подождите, идёт распознавание...").show();
                  new $ws.proto.BLObject("Blyudoglyad").call("Analyze",{"Photo":{"Данные":base64data}}, $ws.proto.BLObject.RETURN_TYPE_RECORDSET).addCallback(function(rs){
                     $ws.single.Indicator.hide();
                     $(".StartPage__video").css("display","block").animate({ left: "0px", height: "450px", width: "620px", top: "20px", margin: "0 90px" }, 700);
                     $(".StartPage__videoOuter").animate({ margin:"0" });
                     $(document.getElementById('recognized')).css("display","block").animate({left: "0px", top: "450px", width: "770px"}, 800);
                     
                     var cont = new $ws.proto.Context();
                     cont.setValue( "rs", rs );
                     
                     //Чек
                     $(document.getElementById('checklist')).css("display","block").animate({ top: "0px", right: "0px", height: "100%", width: "43%"}, 300, 'swing' , function(){
                        new Bill({
                        element: $(document.getElementById('checklist')),
                        context: cont,
                        name: "ResBill"
                     });});
   
                     //Элементы
                     for(var i = 0; i < rs.getRecordCount(); ++i){
                        var id = 'item' + i;
                        $(document.getElementById('recognized')).append( "<div style='display: none' id='" + id + "'></div>" + ( i % 2 === 1 ? "<br/>" : "" ) );
                        var item = new recognizedItem({
                           element: $(document.getElementById(id)),
                           list : rs,
                           id : i,
                           context: cont
                        });
                        $(document.getElementById(id)).fadeIn(3000);
                     }
                  }).addErrback( function(err){
                     $ws.single.Indicator.hide();
                     $ws.core.alert(err.message).addCallback(function(){
                        location.reload();
                     });
                  });
               };
            });
            
         });
         
      },
      
      
      
      
   });
   return moduleClass;
});