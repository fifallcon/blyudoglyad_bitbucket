/**
 * @author Сажин С.В,
 */
define('js!SBIS3.BLYUDOGLYAD.Bill', ['js!SBIS3.CORE.CompoundControl',
'html!SBIS3.BLYUDOGLYAD.Bill',
'css!SBIS3.BLYUDOGLYAD.Bill',
'js!SBIS3.BLYUDOGLYAD.DishList',
'js!SBIS3.CORE.TemplatedArea',
'js!SBIS3.CONTROLS.Button'
],
function(CompoundControl, dotTplFn, css, DishList, TemplArea) {
   /**
    * SBIS3.BLYUDOGLYAD.Bill
    * @class SBIS3.BLYUDOGLYAD.Bill
    * @extends $ws.proto.CompoundControl
    */
   var moduleClass = CompoundControl.extend(/** @lends SBIS3.BLYUDOGLYAD.Bill.prototype */{
      _dotTplFn: dotTplFn,
      $protected: {
         _options: {
         }
      },
      $constructor: function() {
      },

      reload: function(){
         $(document.getElementById('bill_list')).empty();
         var formStringByChar = function(ch, size){
            var res = "";
            for(var g = 0; g < size; g++)
               res += ch;
            return res;
         };
         var itogo = "Итого:";
         var tm = 38;
         var ResultPrice = 0;
         var rc = this.getLinkedContext().getValue("rs");
         for(var i = 0; i < rc.getRecordCount(); i++){
            if( rc.getRecordByPrimaryKey(i).get("@Dish") === 99 )
               continue;
            var Nam = rc.getRecordByPrimaryKey(i).get("Name");
            var CosT = rc.getRecordByPrimaryKey(i).get("Cost");
            var costStr = CosT.toString() + " р.";
            var ttmp = tm - Nam.length - costStr.length;
            ResultPrice += CosT;
            $(document.getElementById('bill_list')).append("<div>" + Nam + formStringByChar(".", ttmp) + costStr + "</div>");
         }
         $(document.getElementById('bill_list')).append(formStringByChar("-", tm));
         var ResultPriceStr = ResultPrice.toString() + " р.";
         tm = tm - itogo.length - ResultPriceStr.length;
         $(document.getElementById('bill_list')).append(itogo + formStringByChar(".", tm) + ResultPriceStr);
      },
      
      init: function() {
         moduleClass.superclass.init.call(this);
         this.reload();
         this.getChildControlByName('Fire').subscribe('onActivated', function(eventObject) {
            $ws.core.alert("Не забудьте пожелать приятного аппетита!", "success").addCallback(function(){
               location.reload();
            });
         });
      }
   });

   return moduleClass;
});