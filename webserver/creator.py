import cv2
import numpy as np
import glob, os

def resizeImg(img, w, h):
	height, width = img.shape[0], img.shape[1]
	new_height = int(w*height / width)
	if new_height > h:
		new_width = int(h*width / height)
		return cv2.resize(img, (new_width, h))
	else:
		return cv2.resize(img, (w, new_height))


os.chdir("C:\\_work\\blyudoglyad\\food_photo")
i = 0
names = {}
for file in glob.glob("*.jpg"):
	print(file)
	
	# load the image
	image = cv2.imread(file)
	#image = resizeImg(image, 900,700)

	gray = cv2.cvtColor(image.copy(), cv2.COLOR_BGR2GRAY) # Конвертируем цветное изображение в монохромное 
	gray_blur = cv2.GaussianBlur(gray, (9, 9), 20) # Добавляем размытие 

	circles = cv2.HoughCircles( gray_blur, cv2.HOUGH_GRADIENT, 1.2, 30)
	# ensure at least some circles were found
	if circles is not None:
		circles = np.round(circles[0, :]).astype("int")
	 
		# loop over the (x, y) coordinates and radius of the circles
		for (x, y, r) in circles:
			i+=1

			crop_img = image[y-r: y+r, x-r: x+r]
			if len(crop_img) > 0:
				try:
					#clahe = cv2.createCLAHE( clipLimit=40.0, tileGridSize=(8,8) )
					#equ = clahe.apply( crop_img )

					#crop_img = resizeImg(equ, 100, 100)
					cv2.imwrite("img\Image_{}.jpg".format(str(i).zfill(3)), crop_img)

					names["Image_{}.jpg".format(str(i).zfill(3))] = ""
				except Exception as e:
					pass

with open("res.txt", "w") as output:
	output.write(str(sorted(names)))
