import cv2
import numpy as np

def resizeImg(img, w, h):
	height, width = img.shape[0], img.shape[1]
	new_height = int(w*height / width)
	if new_height > h:
		new_width = int(h*width / height)
		return cv2.resize(img, (new_width, h))
	else:
		return cv2.resize(img, (w, new_height))

def drop_file(file):
	names = []
	# load the image
	image = cv2.imread(file)
	image = resizeImg(image, 900, 700)
	cv2.imwrite("Image_resized.jpg", image)

	gray = cv2.cvtColor(image.copy(), cv2.COLOR_BGR2GRAY) # Конвертируем цветное изображение в монохромное 
	gray_blur = cv2.GaussianBlur(gray, (9, 9), 20) # Добавляем размытие 

	circles = cv2.HoughCircles( gray_blur, cv2.HOUGH_GRADIENT, 1.2, 30)
	# ensure at least some circles were found
	if circles is not None:
		# convert the (x, y) coordinates and radius of the circles to integers
		circles = np.round(circles[0, :]).astype("int")
	 
		# loop over the (x, y) coordinates and radius of the circles
		i = 0
		for (x, y, r) in circles:
			i+=1

			crop_img = gray[y-r-10: y+r+20, x-r-10: x+r+20]

			try:
				crop_img = resizeImg(crop_img, 200, 200)
				cv2.imwrite("Image_{}.jpg".format(i), crop_img)
				names.append("Image_{}.jpg".format(i))
			except Exception as e:
				pass

	return names