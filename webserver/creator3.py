import cv2
import numpy as np
import glob, os

def resizeImg(img, w, h):
	height, width = img.shape[0], img.shape[1]
	new_height = int(w*height / width)
	if new_height > h:
		new_width = int(h*width / height)
		return cv2.resize(img, (new_width, h))
	else:
		return cv2.resize(img, (w, new_height))

image = cv2.imread('IMG_20161115_163831.jpg',0)
image = resizeImg(image, 900,700)
# mask defaulting to black for 3-channel and transparent for 4-channel
# (of course replace corners with yours)
mask = np.zeros(image.shape, dtype=np.uint8)
roi_corners = np.array([[(10,10), (300,300), (10,300)]], dtype=np.int32)
# fill the ROI so it doesn't get wiped out when the mask is applied
channel_count = image.shape[2]  # i.e. 3 or 4 depending on your image
ignore_mask_color = (255,)*channel_count
cv2.fillPoly(mask, roi_corners, ignore_mask_color)

# apply the mask
masked_image = cv2.bitwise_and(image, mask)

# save the result
cv2.imwrite('image_masked.png', masked_image)