#!C:/Python35/python.exe

from http.server import BaseHTTPRequestHandler, HTTPServer
import urllib.request, urllib.parse, urllib.error
import posixpath
import os
import re

from opencv import drop_file
 
# HTTPRequestHandler class
class testHTTPServer_RequestHandler(BaseHTTPRequestHandler):
    def translate_path(self, path):
        """Translate a /-separated PATH to the local filename syntax.
        Components that mean special things to the local file system
        (e.g. drive or directory names) are ignored.  (XXX They should
        probably be diagnosed.)
        """
        # abandon query parameters
        path = path.split('?',1)[0]
        path = path.split('#',1)[0]
        path = posixpath.normpath(urllib.parse.unquote(path))
        words = path.split('/')
        words = [_f for _f in words if _f]
        path = os.getcwd()
        for word in words:
            drive, word = os.path.splitdrive(word)
            head, word = os.path.split(word)
            if word in (os.curdir, os.pardir): continue
            path = os.path.join(path, word)
        return path

    # GET
    def do_GET(self):
        # Send response status code
        self.send_response(200)

        # Send headers
        self.send_header('Content-type','text/html')
        self.end_headers()

        # Send message back to client
        body = """
        <html>
<head>
    <title>bee</title>
</head>
<body>
<img src="Image_1.jpg">
<form enctype="multipart/form-data" method="post" action="http://127.0.0.1:1111/">
  <input name="file" type="file"/>
  <input type="submit" value="upload"/>
</form>
</body>
</html>
        """
        # Write content as utf-8 data
        self.wfile.write(bytes(body, "utf8"))
        return

    def do_POST(self):
        print("start")
        content_type = self.headers['content-type']
        boundary = content_type.split("=")[1].encode()
        remainbytes = int(self.headers['content-length'])
        line = self.rfile.readline()
        remainbytes -= len(line)
        line = self.rfile.readline()
        remainbytes -= len(line)
        fn = re.findall(r'Content-Disposition.*name="file"; filename="(.*)"', line.decode())
        path = self.translate_path(self.path)
        fn = os.path.join(path, fn[0])
        print("filename: {0}".format(fn))
        line = self.rfile.readline()
        remainbytes -= len(line)
        line = self.rfile.readline()
        remainbytes -= len(line)

        preline = self.rfile.readline()
        remainbytes -= len(preline)
        with open(fn, 'wb') as out:
            while remainbytes > 0:
                line = self.rfile.readline()
                remainbytes -= len(line)
                if boundary in line:
                    preline = preline[0:-1]
                    if preline.endswith(b'\r'):
                        preline = preline[0:-1]
                    out.write(preline)
                    break
                else:
                    out.write(preline)
                    preline = line

        files = drop_file(fn)
        print(files)

        # Send response status code
        self.send_response(200)

        # Send headers
        self.send_header('Content-type','text/html')
        self.end_headers()

        # Send message back to client
        body = """
        <html>
<head>
    <title>{0}</title>
</head>
<body>
{0}
<img src="Image_1.jpg">
<form enctype="multipart/form-data" method="post" action="http://127.0.0.1:1111/">
  <input name="file" type="file"/>
  <input type="submit" value="upload"/>
</form>
</body>
</html>
        """.format(fn)
        # Write content as utf-8 data
        self.wfile.write(bytes(body, "utf8"))
        return
     
 
def run():
    print('starting server...')
 
    # Server settings
    # Choose port 8080, for port 80, which is normally used for a http server, you need root access
    server_address = ('127.0.0.1', 1111)
    httpd = HTTPServer(server_address, testHTTPServer_RequestHandler)
    print('running server...')
    httpd.serve_forever()
 
 
run()