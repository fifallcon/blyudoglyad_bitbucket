import cv2
import numpy as np
import glob, os
import psycopg2

conn = psycopg2.connect("dbname='sbiskassir' user='postgres' host='localhost' password='postgre'")
cursor = conn.cursor()

filenames = {
 'Image_001.jpg':"сухари",
 'Image_002.jpg':"суп1",
 'Image_003.jpg':"картошка_бризоль",
 'Image_004.jpg':"сухари",
 'Image_005.jpg':"картошка_бризоль",
 'Image_006.jpg':"суп1",
 'Image_007.jpg':"суп2",
 'Image_008.jpg':"картошка_бризоль",
 'Image_009.jpg':"бризоль",
 'Image_010.jpg':"бризоль",
 'Image_011.jpg':"салат",
 'Image_012.jpg':"суп1",
 'Image_013.jpg':"бризоль",
 'Image_014.jpg':"суп1",
 'Image_015.jpg':"бризоль",
 'Image_016.jpg':"суп2",
 'Image_017.jpg':"мясофранц",
 'Image_018.jpg':"сухари",
 'Image_019.jpg':"мясофранц",
 'Image_020.jpg':"суп2",
 'Image_021.jpg':"сухари",
 'Image_022.jpg':"суп2",
 'Image_023.jpg':"мясофранц",
 'Image_024.jpg':"сухари",
 'Image_025.jpg':"бризоль",
 'Image_026.jpg':"суп2",
 'Image_027.jpg':"сухари",
 'Image_028.jpg':"бризоль",
 'Image_029.jpg':"суп2",
 'Image_030.jpg':"сухари",
 'Image_031.jpg':"сухари",
 'Image_032.jpg':"суп2",
 'Image_033.jpg':"салат",
 'Image_034.jpg':"суп2",
 'Image_035.jpg':"сухари",
 'Image_036.jpg':"салат",
 'Image_037.jpg':"макароны",
 'Image_038.jpg':"блин",
 'Image_039.jpg':"блин",
 'Image_040.jpg':"макароны",
 'Image_041.jpg':"блин",
 'Image_042.jpg':"макароны",
 'Image_043.jpg':"блин",
 'Image_044.jpg':"макароны",
 'Image_045.jpg':"блин",
 'Image_046.jpg':"блин",
 'Image_047.jpg':"блин",
 'Image_048.jpg':"блин",
 'Image_049.jpg':"макароны",
 'Image_050.jpg':"блин",
 'Image_051.jpg':"блин",
 'Image_052.jpg':"макароны",
 'Image_053.jpg':"блин",
 'Image_054.jpg':"макароны",
 'Image_055.jpg':"блин",
 'Image_056.jpg':"макароны",
 'Image_057.jpg':"блин_блин_сметана",
 'Image_058.jpg':"пирожок",
 'Image_059.jpg':"блин_блин_сметана",
 'Image_060.jpg':"пирожок",
 'Image_061.jpg':"блин_блин_сметана",
 'Image_062.jpg':"пирожок",
 'Image_063.jpg':"пирожок",
 'Image_064.jpg':"блин_блин_сметана",
 'Image_065.jpg':"пирожок",
 'Image_066.jpg':"блин_блин_сметана",
 'Image_067.jpg':"блин_блин_сметана",
 'Image_068.jpg':"пирожок",
 'Image_069.jpg':"блин_блин_сметана",
 'Image_070.jpg':"пирожок",
 'Image_071.jpg':"блин_блин_сметана",
 'Image_072.jpg':"пирожок",
 'Image_073.jpg':"пирожок",
 'Image_074.jpg':"блин_блин_сметана",
 'Image_075.jpg':"пирожок",
 'Image_076.jpg':"блин_блин_сметана",
 'Image_077.jpg':"блин_блин_сметана",
 'Image_078.jpg':"пирожок",
 'Image_079.jpg':"рис",
 'Image_080.jpg':"сосисавтесте",
 'Image_081.jpg':"сосисавтесте",
 'Image_082.jpg':"рис",
 'Image_083.jpg':"рис",
 'Image_083.jpg':"рис",
 'Image_085.jpg':"рис",
 'Image_086.jpg':"сосисавтесте",
 'Image_087.jpg':"рис",
 'Image_088.jpg':"сосисавтесте",
 'Image_089.jpg':"сосисавтесте",
 'Image_090.jpg':"сосисавтесте",
 'Image_091.jpg':"сосисавтесте",
 'Image_092.jpg':"сосисавтесте",
 'Image_093.jpg':"сосисавтесте",
 'Image_094.jpg':"сосисавтесте",
 'Image_095.jpg':"сосисавтесте",
 'Image_096.jpg':"сосисавтесте",
 'Image_097.jpg':"сосисавтесте",
 'Image_098.jpg':"сосисавтесте",
 'Image_099.jpg':"сосисавтесте",
 'Image_100.jpg':"сосисавтесте",
 'Image_101.jpg':"сосисавтесте"
}

os.chdir("C:\\_work\\blyudoglyad\\food_photo\\mini100_2")
i = 0
names = {}
for file in glob.glob("*.jpg"):
	print(file)
	
	# load the image
	image = cv2.imread(file)
	query = """INSERT INTO dishes (image, label) 
	VALUES (ARRAY{0}, '{1}')
	""".format(image, filenames[file])
	cursor.execute(query)

