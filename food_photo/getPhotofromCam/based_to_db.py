import psycopg2
import glob, os
import cv2
import numpy as np

# conn = psycopg2.connect("dbname='sbiskassir' user='postgres' host='85.21.233.63' password='kassir123' port='7777'")
# cursor = conn.cursor()

def save_to_db(img):
    arr = []
    for row in img:
        for pix in row:
            arr.append(int(pix))
    print(len(arr))
    query = """INSERT INTO images (image, label_id, type_id) 
    VALUES (%s, %s, 4)
    """
    cursor.execute(query, (arr, 14))
    conn.commit()

def resizeImg(img, w, h):
    height, width = img.shape[0], img.shape[1]
    new_height = int(w*height / width)
    if new_height > h:
        new_width = int(h*width / height)
        return cv2.resize(img, (new_width, h))
    else:
        return cv2.resize(img, (w, new_height))

def removeBackground(gray):    
    h = gray.shape[0]
    mask = np.ones((h,h))
    mask = np.round(mask).astype("uint8")  
    for i in range(0,h):
        for j in range(0,h):
            if (i-h/2)**2 + (j - h/2)**2 > (h/2)**2:
                mask[i,j] = 0
    gray = gray*mask
    return gray            

def getCircles(circles):
    if circles is not None:
        circles = np.round(circles[0, :]).astype("int")
        lc = circles.shape[0]
        r = np.floor(lc/2)-1
        r = np.round(r).astype("int")
        for k in range(0,lc-1):
            for y in range(0,):
                if (circles[k,0]-circles[k+y],0)**2 + (circles[k,1]-circles[k+y],1)**2 < circles(k,2)**2:
                    np.delete(circles,k,0) 
    return circles       

cap = cv2.VideoCapture(1)
i = 0

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Display the resulting frame
    cv2.imshow('frame',gray)
    key = cv2.waitKey(1) & 0xFF
    if key == ord('p'):
        #gray = cv2.cvtColor(image.copy(), cv2.COLOR_BGR2GRAY) # Конвертируем цветное изображение в монохромное 
        gray_blur = cv2.GaussianBlur(gray, (9, 9), 20) # Добавляем размытие 

        circles = cv2.HoughCircles( gray_blur, cv2.HOUGH_GRADIENT, 1.2, 30)
        # ensure at least some circles were found
        if circles is not None:
            # circles = np.round(circles[0, :]).astype("int")
            circles = getCircles(circles) 
            # loop over the (x, y) coordinates and radius of the circles
            for (x, y, r) in circles:
                i+=1

                crop_img = gray[y-r: y+r, x-r: x+r]
                if len(crop_img) > 0:
                    clahe = cv2.createCLAHE(clipLimit=20.0, tileGridSize=(10,10))
                    crop_img = clahe.apply(crop_img)
                    crop_img = removeBackground(crop_img)

                    try:
                        cv2.imshow("circle"+str(i),crop_img)
                        save_to_db(crop_img)
                        print("SUCCES")
                    except Exception as e:
                        raise e
        else:
            print("BEDA")
    elif key == ord('q'):
        break
# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()