import numpy as np
import cv2
 
im = cv2.imread('im_1.jpg')

height,width,depth = im.shape
circle_img = np.zeros((height,width), np.uint8)
cv2.circle(circle_img,(int(width/2),int(height/2)),40,1,thickness=-1)
masked_data = cv2.bitwise_and(im, im, mask=circle_img)

cv2.imshow("masked", masked_data)
cv2.waitKey(0)