import glob, os
import cv2
import numpy as np

def get_holes(image, thresh):
    gray = image

    im_bw = cv2.threshold(gray, thresh, 255, cv2.THRESH_BINARY)[1]
    im_bw_inv = cv2.bitwise_not(im_bw)

    contour = cv2.findContours(im_bw_inv, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)[0]
    for cnt in contour:
        ctr = np.array(cnt).reshape((-1,1,2)).astype(np.int32)
        cv2.drawContours(im_bw_inv, [ctr], 0, 255, -1)

    nt = cv2.bitwise_not(im_bw)
    im_bw_inv = cv2.bitwise_or(im_bw_inv, nt)
    return im_bw_inv


def remove_background(image, thresh, scale_factor=.25, kernel_range=range(1, 15), border=None):
    border = border or kernel_range[-1]

    holes = get_holes(image, thresh)
    small = cv2.resize(holes, None, fx=scale_factor, fy=scale_factor)
    bordered = cv2.copyMakeBorder(small, border, border, border, border, cv2.BORDER_CONSTANT)

    for i in kernel_range:
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2*i+1, 2*i+1))
        bordered = cv2.morphologyEx(bordered, cv2.MORPH_CLOSE, kernel)

    unbordered = bordered[border: -border, border: -border]
    mask = cv2.resize(unbordered, (image.shape[1], image.shape[0]))
    fg = cv2.bitwise_and(image, image, mask=mask)
    return fg




#remove_background дает баг, показать его
#def remove_bakground_stupid(image,thresh)
 #   holes = get_holes(image,thresh) 











def resizeImg(img, w, h):
    height, width = img.shape[0], img.shape[1]
    new_height = int(w*height / width)
    if new_height > h:
        new_width = int(h*width / height)
        return cv2.resize(img, (new_width, h))
    else:
        return cv2.resize(img, (w, new_height))


# Capture frame-by-frame
img = cv2.imread('test.jpg')

    # Our operations on the frame come here
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Display the resulting frame
while(True):
    cv2.imshow('frame',img)

    gray = resizeImg(gray, 900,900)
    img = resizeImg(img, 900,900)

    gray_blur = cv2.GaussianBlur(gray, (9, 9), 20) # Добавляем размытие 

    i = 0
    some = 100

    circles = cv2.HoughCircles( gray_blur, cv2.HOUGH_GRADIENT, 1.2, 20)
        # ensure at least some circles were found
    if circles is not None:
        circles = np.round(circles[0, :]).astype("int")
         
    # loop over the (x, y) coordinates and radius of the circles
        for (x, y, r) in circles:
            i+=1

            crop_img = gray[y-r: y+r, x-r: x+r]
            if len(crop_img) > 0:
                    #crop_img = resizeImg(crop_img, 200,200)
                    #height, width = crop_img.shape[0], crop_img.shape[1]
                    #circle_img = np.zeros((height,width), np.uint8)
                    #cv2.circle(circle_img,(int(width/2),int(height/2)),int(height/2),1,thickness=-1)
                    #crop_img = cv2.bitwise_and(crop_img, crop_img, mask=circle_img)
                    
                try:
                        #lab= cv2.cvtColor(crop_img, cv2.COLOR_BGR2LAB)
                        #l, a, b = cv2.split(lab)
                        
                    nb_img = remove_background(crop_img, some)
                    cv2.imshow("im"+str(i), nb_img)
                except Exception as e:
                    raise e                    
    else:
        print("BEDA")
    if cv2.waitKey(1) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break
print(circles)
# When everything done, release the capture