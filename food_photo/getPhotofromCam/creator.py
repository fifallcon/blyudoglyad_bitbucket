import numpy as np
import cv2

def resizeImg(img, w, h):
    height, width = img.shape[0], img.shape[1]
    new_height = int(w*height / width)
    if new_height > h:
        new_width = int(h*width / height)
        return cv2.resize(img, (new_width, h))
    else:
        return cv2.resize(img, (w, new_height))

def save_many_photo(img, i, r):
    height,width = img.shape
    circle_img = np.zeros((height,width), np.uint8)
    cv2.circle(circle_img,(int(width/2),int(height/2)),r,1,thickness=-1)
    img = cv2.bitwise_and(img, img, mask=circle_img)

    for j in range(360):
        rows,cols = img.shape[0], img.shape[1]
        M = cv2.getRotationMatrix2D((cols/2,rows/2),j,1)
        dst = cv2.warpAffine(img,M,(cols,rows))

        dst = resizeImg(dst, 100, 100)
        cv2.imwrite("mini\\im_{0}_{1}.jpg".format(str(i).zfill(3), str(j).zfill(3)), dst)

cap = cv2.VideoCapture(1)
i = 0

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Display the resulting frame
    cv2.imshow('frame',gray)
    if cv2.waitKey(1) & 0xFF == ord('p'):
        gray = resizeImg(gray, 900,700)
        #gray = cv2.cvtColor(image.copy(), cv2.COLOR_BGR2GRAY) # Конвертируем цветное изображение в монохромное 
        gray_blur = cv2.GaussianBlur(gray, (9, 9), 20) # Добавляем размытие 

        circles = cv2.HoughCircles( gray_blur, cv2.HOUGH_GRADIENT, 1.2, 30)
        # ensure at least some circles were found
        if circles is not None:
            circles = np.round(circles[0, :]).astype("int")
         
            # loop over the (x, y) coordinates and radius of the circles
            for (x, y, r) in circles:
                i+=1

                crop_img = gray[y-r: y+r, x-r: x+r]
                if len(crop_img) > 0:
                    clahe = cv2.createCLAHE(clipLimit=20.0, tileGridSize=(10,10))
                    crop_img = clahe.apply(crop_img)

                    try:
                        save_many_photo(crop_img, i, r)
                    except Exception as e:
                        raise e
        else:
            print("BEDA")

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()