import cv2
import numpy as np
a = cv2.imread('test2.jpg')
gray = cv2.cvtColor(a,cv2.COLOR_BGR2GRAY)
h = gray.shape[0]
mask = np.ones((h,h))
circles = cv2.HoughCircles( gray, cv2.HOUGH_GRADIENT, 1.2, 20) 
print(circles.shape)
circles = np.round(circles[0, :]).astype("int")  
mask = np.round(mask).astype("int")  

for i in range(0,h):
	for j in range(0,h):
		if (i-circles[0,0])**2 + (j - circles[0,1])**2 > circles[0,2]**2:
			mask[i,j] = 0


gray = gray*mask
while(True):
	cv2.imshow("gray",gray)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break
cv2.imwrite('result.png',gray)
cv2.destroyAllWindows()        